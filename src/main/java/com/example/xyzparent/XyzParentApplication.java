package com.example.xyzparent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XyzParentApplication {

    public static void main(String[] args) {
        SpringApplication.run(XyzParentApplication.class, args);
    }

}
